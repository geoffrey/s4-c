/* Comparaison de deux tableaux d'entiers */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

    int T1[] = {12, 23, 34, 45, 56, 67, 78, 89, 90};
    int T2[] = {12, 23, 34, 45, 56, 67, 78, 89, 90};


    if (sizeof(T1) != sizeof(T2)) {
        printf("Les tableaux ne sont pas de la même taille.\n");
        exit(EXIT_SUCCESS);
    }
    int n = sizeof(T1)/sizeof(int);
    int *P1, *P2;
    for (P1 = T1, P2 = T2; P1 < T1 + n; P1++, P2++) {
        if (*P1 != *P2) {
            printf("Les tableaux diffèrent.\n");
            exit(EXIT_SUCCESS);
        }
    }
    printf("Les tableaux sont identiques.\n");
    exit(EXIT_SUCCESS);
    return 0;
}
