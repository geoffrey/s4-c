/* Inversion de tableau */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

    int T[] = {12, 23, 34, 45, 56, 67, 78, 89, 90};
    int *P1, *P2, temp, n = sizeof(T)/sizeof(int);

    P2 = T + n - 1;
    for (P1 = T; P1 < T + n/2; P1++, P2--) {
        temp = *P1;
        *P1 = *P2;
        *P2 = temp;
    }

    for (P1 = T; P1 < T + n; P1++) {
        printf("%d, ", *P1);
    }
    printf("\n");

    return 0;
}
