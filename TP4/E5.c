/* Recherche de la valeur minimale */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
    int T[] = {90, 23, 34, 10, 56, 67, 78, 89, 12};
    int deb = 0;
    int fin = 8;

    int *P, *minI = T + deb, min = *(minI);
    for (P = T + deb; P <= T + fin; P++) {
        if (*P < min) {
            min = *P;
            minI = P;
        }
    }
    printf("La valeur minimum est %d et est à l'indice %d.\n", min, minI-T);

    return 0;
}
