/* Tri */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
    int n, *P1, *P2, min, *minP;
    printf("Saississez la taille du tableau : ");
    scanf("%d", &n);

    int T[n];
    for (P1 = T; P1 < T+n; P1++) {
        printf("T[%d] = ", P1-T);
        scanf("%d", P1);
    }

    for (P1 = T; P1 < T+n; P1++) {
        for (P2 = P1; P2 < T+n; P2++) {
            if (*P2 < min || P2 == P1) {
                min = *P2;
                minP = P2;
            }
        }
        *minP = *P1;
        *P1 = min;
    }

    // Affichage
    for (P1 = T; P1 < T+n; P1++) {
        printf("%d, ", *P1);
    }
    printf("\n");

    return 0;
}
