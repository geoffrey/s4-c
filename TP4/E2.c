/* Retirer les éléments d'un tableau */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    int X = 0, n, *P1, *P2;
    printf("Saississez la taille de la chaîne : ");
    scanf("%d", &n);
    int T[n];
    for (P1 = T; P1 < T+n; P1++) {
        *P1 = 0;
        printf("Saississez le caractère à l'indice %d : ", P1-T);
        scanf("%s", P1);
    }
    printf("Saississez la caractère X : ");
    scanf("%s", &X);

    P2 = T;
    for (P1 = T; P1 < T+n; P1++) {
        if (*P1 != X) {
            *P2 = *P1;
            P2++;
        }
    }
    *P2 = '\0';

    for (P1 = T; P1 < T+n; P1++) {
        if (*P1 == '\0') {
            break;
        }
        printf("%s", P1);
    }
    printf("\n");

    return 0;
}
