/* Pointeurs sur tableau */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    int T[] = {12, 23, 34, 45, 56, 67, 78, 89, 90};
    int *P;
    P=T;

    // Référence
    printf("T = %d ; P = %d\n", T, P); // T = 557221504 ; P = 557221504
    printf("&T = %d ; &P = %d\n", &T, &P); // &T = 557221504 ; &P = 557221496
    printf("*T = %d ; *P = %d\n", *T, *P); // *T = 12 ; *P = 12

    // Questions
    printf("%d\n", *P+2); // 14
    printf("%d\n", &T[4]-3); // 557221508
    printf("%d\n", P+(*P-10)); // 557221512
    printf("%d\n", *(P+2)); // 34
    printf("%d\n", T+3); // 557221516
    printf("%d\n", *(P+*(P+8)-T[7])); // 23
    printf("%d\n", &P+1); // 557221504
    printf("%d\n", &T[7]-P); // 7

    return 0;
}
