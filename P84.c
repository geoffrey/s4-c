/* Allocations dynamiques */

#include <stdlib.h>
#include <stdio.h>

void init(int *tab, int n) {
    int i;
    for (i = 0; i < n; i++) {
        tab[i] = i;
        printf("tab[%d]=%d\n", i, tab[i]);
    }
}

int main() {
    int i, n, *tab;
    printf("Saisissez n : ");
    scanf("%d", &n);
    tab = (int *) malloc(n * sizeof(int));
    init(tab, n);

    return 0;
}

