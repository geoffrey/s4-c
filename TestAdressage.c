/* Test addressage au tableau */

#include<stdio.h>

int main() {
    int a = 1, b, c;
    b = ++a;
    printf("L'adresse de a est %u\n", &a);
    printf("L'adresse de b est %u\n", &b);
    return 0;
}
