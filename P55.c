/* Exemple p54 */

#include <stdio.h>

int main() {
    unsigned int x, y, z, w;
    x = 1;
    y = (x << 3);
    z = 10;
    w = (z >> 1);
    printf("x, y, z, w = %d, %d, %d, %d\n", x, y, z, w);
    return 0;
}
