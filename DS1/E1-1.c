/* Nombre triangulaire */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
    int n, i, j, T = 0;
    printf("Saisissez n : ");
    scanf("%d", &n);
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= i; j++) {
            printf("*");
            T += 1;
        }
        printf("\n");

    }
    // printf("T_%d = %d\n", n, n*(n+1)/2);
    printf("T_%d = %d\n", n, T);

    return 0;
}
