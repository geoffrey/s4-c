/* Stockage de matrice symétrique */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {

    // 1)

    // Saisie de la matrice
    int n, i, j;
    printf("Saisissez n : ");
    scanf("%d", &n);
    double A[n][n];
    for (j = 1; j <= n; j++) {
        for (i = 1; i <= n; i++) {
            printf("Saisissez l'élément aux coordonnées (%d ; %d) : ", i, j);
            scanf("%lf", &A[i-1][j-1]);
        }
    }

    // Affichage de la matrice
    for (j = 1; j <= n; j++) {
        for (i = 1; i <= n; i++) {
            printf("%11f ", A[i-1][j-1]);
        }
        printf("\n");
    }

    // Verification de la symétrie
    for (j = 1; j <= n; j++) {
        for (i = j+1; i <= n; i++) {
            if (A[i-1][j-1] != A[j-1][i-1]) {
                printf("La matrice n'est pas symétrique.\n");
                exit(EXIT_FAILURE);
            }
        }
    }

    // Création du vecteur U
    int k, m, M, K = n*(n+1)/2;
    double U[K];
    for (i = 1; i <= n; i++) {
        for (j = i; j <= n; j++) {
            M = 0;
            for (m = 1; m <= i - 2; m++) {
                M += m;
            }
            k = (i - 1) * (n - 1) + j - M;
            U[k-1] = A[i-1][j-1];
        }
    }

    // Affichage du vecteur U
    printf("U = {");
    for (k = 1; k <= K; k++) {
        printf("%f ", U[k-1]);
    }
    printf("}\n");

    return 0;
}

// 2)
//
// Saisissez n : 6
// Saisissez l'élément aux coordonnées (1 ; 1) : -2
// Saisissez l'élément aux coordonnées (2 ; 1) : 1
// Saisissez l'élément aux coordonnées (3 ; 1) : 3
// Saisissez l'élément aux coordonnées (4 ; 1) : -1
// Saisissez l'élément aux coordonnées (5 ; 1) : 4
// Saisissez l'élément aux coordonnées (6 ; 1) : 1
// Saisissez l'élément aux coordonnées (1 ; 2) : 1
// Saisissez l'élément aux coordonnées (2 ; 2) : -1
// Saisissez l'élément aux coordonnées (3 ; 2) : 5
// Saisissez l'élément aux coordonnées (4 ; 2) : 2
// Saisissez l'élément aux coordonnées (5 ; 2) : 0
// Saisissez l'élément aux coordonnées (6 ; 2) : 7
// Saisissez l'élément aux coordonnées (1 ; 3) : 3
// Saisissez l'élément aux coordonnées (2 ; 3) : 5
// Saisissez l'élément aux coordonnées (3 ; 3) : -3
// Saisissez l'élément aux coordonnées (4 ; 3) : 6
// Saisissez l'élément aux coordonnées (5 ; 3) : -2
// Saisissez l'élément aux coordonnées (6 ; 3) : 8
// Saisissez l'élément aux coordonnées (1 ; 4) : -1
// Saisissez l'élément aux coordonnées (2 ; 4) : 2
// Saisissez l'élément aux coordonnées (3 ; 4) : 6
// Saisissez l'élément aux coordonnées (4 ; 4) : -4
// Saisissez l'élément aux coordonnées (5 ; 4) : 8
// Saisissez l'élément aux coordonnées (6 ; 4) : -2
// Saisissez l'élément aux coordonnées (1 ; 5) : 4
// Saisissez l'élément aux coordonnées (2 ; 5) : 0
// Saisissez l'élément aux coordonnées (3 ; 5) : -2
// Saisissez l'élément aux coordonnées (4 ; 5) : 8
// Saisissez l'élément aux coordonnées (5 ; 5) : 1
// Saisissez l'élément aux coordonnées (6 ; 5) : -5
// Saisissez l'élément aux coordonnées (1 ; 6) : 1
// Saisissez l'élément aux coordonnées (2 ; 6) : 7
// Saisissez l'élément aux coordonnées (3 ; 6) : 9
// Saisissez l'élément aux coordonnées (4 ; 6) : -2
// Saisissez l'élément aux coordonnées (5 ; 6) : -5
// Saisissez l'élément aux coordonnées (6 ; 6) : -6
//   -2.000000    1.000000    3.000000   -1.000000    4.000000    1.000000 
//    1.000000   -1.000000    5.000000    2.000000    0.000000    7.000000 
//    3.000000    5.000000   -3.000000    6.000000   -2.000000    9.000000 
//   -1.000000    2.000000    6.000000   -4.000000    8.000000   -2.000000 
//    4.000000    0.000000   -2.000000    8.000000    1.000000   -5.000000 
//    1.000000    7.000000    9.000000   -2.000000   -5.000000   -6.000000 
// U = {-2.000000 1.000000 3.000000 -1.000000 4.000000 1.000000 -1.000000 5.000000 2.000000 0.000000 7.000000 -3.000000 6.000000 -2.000000 9.000000 -4.000000 8.000000 -2.000000 1.000000 -5.000000 -6.000000 }
