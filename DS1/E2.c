/* Capitalisation */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {

    // 1)

    // Saisie

    double C_0, C, tTot = 0, tMoy;
    int m, k, i, nTot = 0;
    printf("Saisissez C_0 le capital placé : ");
    scanf("%lf", &C_0);
    printf("Saisissez m le nombre d'années de placement : ");
    scanf("%d", &m);

    printf("Saisissez k le nombre de changements de taux : ");
    scanf("%d", &k);
    int n[k];
    double t[k];
    for (i = 1; i <= k; i++) {
        printf("Saisissez n_%d le nombre d'années que dure le taux : ", i);
        scanf("%d", &n[i-1]);
        printf("Saisissez t_%d le taux : ", i);
        scanf("%lf", &t[i-1]);
        nTot += n[i-1];
        tTot += t[i-1] * n[i-1];
    }
    if (nTot != m) {
        printf("Somme des années de changements de captaux incorrecte, %d attendu, %d obtenu.\n", m, nTot);
        exit(EXIT_FAILURE);
    }
    tMoy = tTot / nTot;
    printf("Taux moyen = %f\n", tMoy);

    // Calcul

    printf("Par réccurence : \n");
    C = C_0;
    for (i = 1; i <= k; i++) {
        C *= pow(1 + t[i-1], n[i-1]);
    }
    printf("C = %f\n", C);

    // 3)

    printf("Avec le taux moyen : \n");
    C = C_0*pow(1 + tMoy, nTot);
    printf("C = %f\n", C);

    // On constate que le C calculé est proche, mais des erreurs liées à la précision
    // des nombres en virgule flottantes ont eu lieu

    return 0;
}

// 2)
//
// Saisissez C_0 le capital placé : 10000
// Saisissez m le nombre d'années de placement : 7
// Saisissez k le nombre de changements de taux : 4
// Saisissez n_1 le nombre d'années que dure le taux : 2
// Saisissez t_1 le taux : 0.0225
// Saisissez n_2 le nombre d'années que dure le taux : 3
// Saisissez t_2 le taux : 0.015
// Saisissez n_3 le nombre d'années que dure le taux : 1
// Saisissez t_3 le taux : 0.0175
// Saisissez n_4 le nombre d'années que dure le taux : 1
// Saisissez t_4 le taux : 0.02
// C = 11346.432916
