/* Triangle de Floyd */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
    int n, i, j, t = 1, S, Sa;
    printf("Saisissez n : ");
    scanf("%d", &n);
    for (i = 1; i <= n; i++) {
        S = 0;
        for (j = 1; j <= i; j++) {
            printf("%11d ", t);
            S += t;
            t++;
        }
        printf("\n");

        Sa = i*(pow(i, 2) + 1)/2;

        if (S != Sa) {
            printf("Somme incorrecte, %d attendu, %d obtenu.\n", Sa, S);
            exit(EXIT_FAILURE);
        }

    }
    return 0;
}
