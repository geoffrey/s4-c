/* Séquence de Caterer */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[]) {
    int n, C, i;
    printf("Saisissez n : ");
    scanf("%d", &n);

    // Réccurence
    C = 1;
    printf("Par réccurence :\n");
    printf("C_0 = 1\n");
    for (i = 1; i <= n; i++) {
        C = i + C;
        printf("C_%d = %d\n", i, C);
    }

    // Formule
    printf("Par formule :\n");
    for (i = 0; i <= n; i++) {
        C = (pow(i, 2) + i + 2)/2;
        printf("C_%d = %d\n", i, C);
    }

    return 0;
}
