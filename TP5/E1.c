/* Composition de fonctions */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double f(double x) {
    return pow(x, 3) + 2 * pow(x, 2) + 3 * x - 4;
}

double g(double x) {
    return 1 - x;
}

double composition(double(*Pf)(double x), double(*Pg)(double x), double x) {
    return (*Pf)((*Pg)(x));
}

int main(int argc, char *argv[]) {
    double(*Pf)(double x) = &f;
    double(*Pg)(double x) = &g;
    double a = -1;
    printf("fog(%lf) = %lf\n", a, composition(Pf, Pg, a));
    printf("gof(%lf) = %lf\n", a, composition(Pg, Pf, a));
    printf("fof(%lf) = %lf\n", a, composition(Pf, Pf, a));
    printf("gog(%lg) = %lg\n", a, composition(Pg, Pg, a));
    return 0;
}
