/* Inverse de la matrice de Hilbert */

#include <stdio.h>
#include <math.h>

int facto(int n);

int facto(int n) {
    // Correct pour n ≤ 12
    if (n <= 1) {
        return 1;
    } else {
        return n*facto(n-1);
    }
}

int main() {
    int n;
    printf("Saisissez n : ");
    scanf("%d", &n);
    int i, j;
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= n; j++) {
            printf("%11d ", (pow(-1, i+j)*facto(n+i-1)*facto(n+j-1))/((i+j-1)*pow(facto(i-1)*facto(j-1), 2)*facto(n-i)*facto(n-j)));
        }
        printf("\n");
    }

    return 0;
}

// 3)
//
// Saisissez n : 4
//  1072693248  1072693248  1073741824  1075314688 
//  1072693248  1072693248  1073741824  1075314688 
//  1073741824  1073741824  1074790400  1076363264 
//  1075314688  1075314688  1076363264  1078067200
// 
// Saisissez n : 5
//  1072693248  1072693248  1073741824  1075314688  1077411840 
//  1072693248  1072693248  1073741824  1075314688  1077411840 
//  1073741824  1073741824  1074790400  1076363264  1078460416 
//  1075314688  1075314688  1076363264  1078067200  1080164352 
//  1077411840  1077411840  1078460416  1080164352  1082261504
