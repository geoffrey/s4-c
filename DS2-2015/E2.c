/* Dates */

#include <stdio.h>

int horos(int J, int M, int A);
int bissextile(int A);
int zeller(int J, int M, int A);

int horos(int J, int M, int A) {
    switch (M) {
        case 1:
            return J < 20 ? 9 : 10;
        case 2:
            return J < 19 ? 10 : 11;
        case 3:
            return J < 21 ? 11 : 0;
        case 4:
            return J < 20 ? 0 : 1;
        case 5:
            return J < 21 ? 1 : 2;
        case 6:
            return J < 21 ? 2 : 3;
        case 7:
            return J < 22 ? 3 : 4;
        case 8:
            return J < 23 ? 4 : 5;
        case 9:
            return J < 23 ? 5 : 6;
        case 10:
            return J < 23 ? 6 : 7;
        case 11:
            return J < 22 ? 7 : 8;
        case 12:
            return J < 22 ? 8 : 9;
        default:
            return -1;
    }
}

int bissextile(int A) {
    if (A % 4) { // Pas divisible
        return 0;
    } else { // Divisible
        if (A % 100) {
            return 1;
        } else {
            if (A % 400) {
                return 0;
            } else {
                return 1;
            }
        }
    }
}

int zeller(int J, int M, int A) {
    double X, Y, Z, B;
    if (M <= 2) {
        X = M + 10;
        B = A - 1;
    } else {
        X = M - 2;
        B = A;
    }
    Z = (int) (B / 100);
    Y = B - 100 * Z;

    return (int) (J + (int) (2.6 * X - 0.2) + Y + (int) (Y / 4) + (int) (Z / 4) + 5 * Z) % 7;
}

int main() {
    int J, M, A;

    printf("Saisissez l'année : ");
    scanf("%d", &A);
    if (A <= 1582) {
        printf("À cette date là, le calendrier grégorien n'était pas encore établi, les résultats suivants ne sont donc peut-être pas exacts.\n");
    }

    printf("Saisissez le mois : ");
    scanf("%d", &M);
    if (M < 1 || M > 12) {
        printf("Les mois vont de 1 à 12 !\n");
        return 2;
    }

    printf("Saisissez le jour : ");
    scanf("%d", &J);
    if (J < 1 || M > 31) {
        printf("Les jours vont de 0 à 31 (et encore) !\n");
        return 2;
    }
    if (M == 2) {
        if (J > 29) {
            printf("Les mois de février ont au maximum 29 jours !\n");
            return 2;
        } else if (J == 29 && !bissextile(A)) {
            printf("Cette année n'est pas bissextile, le mois de février n'a donc que 28 jours !\n");
            return 2;
        }
    }

    // TODO

    return 0;
}
