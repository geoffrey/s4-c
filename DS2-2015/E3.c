/* Valeurs approchées d'intégrales */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double f(double x) {
    return exp(2*x) + log(x + 1);
}

// 1)

int main1(int argc, char *argv[]) {
    int n = 100;
    double a = 0;
    double b = 1;
    double h = (b - a) / n;


    // Ici I0s et I1s sont les valeurs approchées
    double I0s = 0;
    double I1s = 0;
    int i;
    // x_i = h * i
    for (i = 0; i < n; i++) {
        I0s += (h/2)*(f(h*i)+f(h*(i+1)));
        I1s += (h/6)*(f(h*i)+4*f(h*i+(h/2)+f(h*(i+1))));
    }
    printf("I0 = %lf\n", I0s);
    printf("I1 = %lf\n", I1s);

    return 0;
}

// 2)

// Ici I0 et I1 sont des fonctions calculant un élément de la somme pour un x_i donné
double I0(double (*Pf)(double x), double a, double h) {
    return (h/2) * ((*Pf)(a) + (*Pf)(a+h));
}

double I1(double (*Pf)(double x), double a, double h) {
    return (h/6) * ((*Pf)(a) + 4*(*Pf)(a+(h/2)) + (*Pf)(a+h));
}

double (*integ[2])(double (*Pf)(double x), double a, double h) = {I0, I1};

int main(int argc, char *argv[]) {
    // Intervalle d'intégration
    double a;
    printf("a = ");
    scanf("%lf", &a);
    double b;
    printf("b = ");
    scanf("%lf", &b);
    // Nombre de pas d'intégration
    int n;
    printf("n = ");
    scanf("%d", &n);
    double h = (b - a) / n;

    // On choisit ici la fonction qu'on veut tester
    double (*Pf)(double x) = f;

    // Ici I0s et I1s sont les valeurs approchées
    double I0s = 0;
    double I1s = 0;
    int i;
    // x_i = h * i
    for (i = 0; i < n; i++) {
        I0s += integ[0](Pf, h*i, h);
        I1s += integ[1](Pf, h*i, h);
    }
    printf("I0 = %lf\n", I0s);
    printf("I1 = %lf\n", I1s);

}
