#TP Langage de programmation en C
##Licence Sciences et Technologies - Semestre 4
Université de Lille 1

Ce dépôt contient les TP et les exemples du cours de cette UE executés par un élève.

###Notes
Les fichiers `Makefile` contiennent les commandes de compilation pour les différents fichiers (sous Windows & Linux).
