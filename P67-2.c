/* Tableau avec initialisation */

#include <stdio.h>

#define N 5

double T[N] = {-2, -1, 0, 1, 3};

int main() {
    int i;
    for (i = 0; i < N; i++)
        printf("T[%d] = %lf\n", i, T[i]);

    return 0;
}

