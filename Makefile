all: $(patsubst %.c,%.exe,$(shell ls *.c))

%.exe: %.c
	gcc $< -o $@
	
.PHONY: all clean

clean:
	rm *.exe
