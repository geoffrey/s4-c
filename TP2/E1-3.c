/* Triangles pythagoriques */
#include <stdio.h>
#include <math.h>

int main() {

    long double h, a = 0;
    long unsigned int n, i = 0;

    printf("Combien de triplets remarquables voulez-vous ? ");
    scanf("%u", &n);
    
    do {
        a++;
        h = sqrt(2*pow(a, 2) + 2*a + 1);
        if (h - (int) h == 0) { // Si h est un entier
            printf("(%llf, %llf, %llf)\n", a, a+1, h);
            i++;
        }

    } while (i < n); 

	return 0;
}
