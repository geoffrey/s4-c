/* Structures et tableaux */

#include <stdio.h>
#include <string.h>

#define M 50 // Maximum d'étudiants stockables
#define NB_UE 6 // Maximum d'étudiants stockables


struct PEIP2 {
    char nom[26], prenom[26];
    long int nip;
    float notes[NB_UE];
} etudiant[M]; 

float coeffs[NB_UE] = {1, 2, 1, 2, 3, 1};

void saisie(int i) {
    printf("Nom : ");
    scanf("%s", &etudiant[i].nom);
    printf("Prénom : ");
    scanf("%s", &etudiant[i].prenom);
    printf("NIP : ");
    scanf("%ld", &etudiant[i].nip);
    int iu;
    for (iu = 0; iu < NB_UE; iu++) {
        printf("Note dans l'UE %i (coeff %f) :", iu+1, coeffs[iu]);
        scanf("%f", &etudiant[i].notes[iu]);
    }
    printf("\n");
}

void lire(int i) {
    printf("Nom, Prénom : %s, %s\n", etudiant[i].nom, etudiant[i].prenom);
    printf("NIP : %ld\n", etudiant[i].nip);
    int iu;
    double somme = 0;
    double sommeCoeffs = 0;
    for (iu = 0; iu < NB_UE; iu++) {
        somme += etudiant[i].notes[iu]*coeffs[iu];
        sommeCoeffs += coeffs[iu];
        printf("Note dans l'UE %i (coeff %f) : %f\n", iu+1, coeffs[iu], etudiant[i].notes[iu]);
    }
    printf("Moyenne générale : %f\n", somme / sommeCoeffs);
    printf("\n");
}


int main() {

    int i, nb;
    printf("Nombre d'étudiants : ");
    scanf("%d", &nb);
    if (nb < 1 || nb > M) {
        printf("Le programme ne peut stocker que jusque %i étudiants.\n", M);
        return 2;
    }

    printf("-- Saisie des étudiants\n");
    for (i = 0; i < nb; ++i) saisie(i);

    printf("-- Lecture des étudiants\n");
    for (i = 0; i < nb; ++i) lire(i);


    printf("-- Moyennes des UE\n");
    int iu;
    double sommeTotale = 0;
    double somme;
    for (iu = 0; iu < NB_UE; iu++) {
        somme = 0;
        for (i = 0; i < nb; ++i) {
            somme += etudiant[i].notes[iu];
        }
        printf("Moyenne de l'UE %i (coeff %f): %f\n", iu+1, coeffs[iu], somme/nb);
        sommeTotale += somme*coeffs[iu];
    }
    printf("Moyenne générale promo : %f\n", sommeTotale / NB_UE / nb);

    return 0;
}
