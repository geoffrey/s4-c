/* Résolution d'un système à matrice triangulaire */

#include <stdio.h>
#include <stdbool.h>

#define N 10 // Ordre maximal

void afficherMatrice(float A[N][N], int n) {
    int i, j;
    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++) {
            printf("%10f ", A[i][j]);
        }
        printf("\n");
    }
}
void afficherVecteur(float Y[N], int n) {
    int i;
    for (i = 0; i < n; i++) {
        printf("%10f ", Y[i]);
    }
    printf("\n");
}

bool estTriangulaireSuperieure(float A[N][N], int n) {
    int i, j;
    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++) {
            if (i > j && A[j][i] != 0) {
                return false;
            }
        }
    }
    return true;
}

bool estTriangulaireInferieure(float A[N][N], int n) {
    int i, j;
    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++) {
            if (i < j && A[j][i] != 0) {
                return false;
            }
        }
    }
    return true;
}

int main() {
    
    int n;
    printf("Saisir l'ordre du système : ");
    scanf("%i", &n);
    if (n < 1 || n > N) {
        printf("L'ordre doit être compris entre 1 et %i", N);
    }

    float A[N][N];
    float X[N], Y[N];
    int i, j;

    printf("-- Saisie de la matrice A\n");
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("Élément de la matrice en (%i, %i) : ", i, j);
            scanf("%f", &A[j][i]);
        }
    }
    printf("-- Saisie du vecteur Y\n");
    for (i = 0; i < n; i++) {
        printf("Élément du vecteur en (%i) : ", i);
        scanf("%f", &Y[i]);
    }
    printf("\n");


    printf("-- Affichage de la matrice A\n");
    afficherMatrice(A, n);
    printf("-- Affichage du vecteur Y\n");
    afficherVecteur(Y, n);

    if (estTriangulaireSuperieure(A, n)) {
        printf("-- Matrice triangulaire supérieure\n");
        X[n-1] = Y[n-1] / A[n-1][n-1];
        for (i = n - 2; i >= 0; i--) {
            float s = 0;
            for (j = i + 1; j < n; j++) {
                s += A[j][i] * X[j];
            }
            X[i] = (Y[i] - s) / A[i][i];
        }

    } else if (estTriangulaireInferieure(A, n)) {
        printf("-- Matrice triangulaire inférieure\n");
        X[0] = Y[0] / A[0][0];
        for (i = 1; i < n; i++) {
            float s = 0;
            for (j = 0; j < i; j++) {
                s += A[j][i] * X[j];
            }
            X[i] = (Y[i] - s) / A[i][i];
        }

    } else {
        printf("La matrice doit être triangulaire (supérieure ou inférieure).\n");
        return 2;
    }

    printf("\n");
    printf("-- Affichage du vecteur X\n");
    afficherVecteur(X, n);

    return 0;
}
