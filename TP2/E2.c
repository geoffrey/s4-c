/* Insérer un élément dans un tableau */

#include<stdio.h>
#define N 10

int main() {
    
    int T[N]; // On crée un tableau (mais on se fiche de son contenu)
    int i, k, X;

    for (i = 0; i < N; i++) {
        printf("%d, ", T[i]);
    }
    printf("\n");
    

    printf("Où insérer un élément ? ");
    scanf("%d", &k);
    if (k < 0 || k > N) {
        printf("L'endroit où insérer l'élément doit être dans le tableau (taille = %d).\n", N);
        return 2;
    }

    printf("Quel élément insérer ? ");
    scanf("%d", &X);

    int Tp[N+1];

    int ip;
    i = 0;
    for (ip = 0; ip < N+1; ip++) {
        if (ip == k) {
            Tp[ip] = X;
        } else {
            Tp[ip] = T[i];
            i++;
        }
    }

    for (i = 0; i < N+1; i++) {
        printf("%d, ", Tp[i]);
    }
    printf("\n");

    // Ici j'ai crée un deuxième tableau Tp au lieu d'insérer X directement dans T
    // On aurait aussi pu parcourir le tableau T à l'envers et décaler les éléments
    // vers la droite jusqu'à k.
    
    return 0;
}
