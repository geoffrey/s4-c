/* Cotés d'un triangle */
#include <stdio.h>

int main() {

    double A, B, C;

    printf("Entrez la longueur du coté A : ");
    scanf("%lf", &A);
    if (A <= 0) {
        printf("La longueur d'un coté doit être strictement supérieure à 0.\n");
        return 0;
    }
    printf("Entrez la longueur du coté B : ");
    scanf("%lf", &B);
    if (B <= 0) {
        printf("La longueur d'un coté doit être strictement supérieure à 0.\n");
        return 0;
    }
    printf("Entrez la longueur du coté C : ");
    scanf("%lf", &C);
    if (C <= 0) {
        printf("La longueur d'un coté doit être strictement supérieure à 0.\n");
        return 0;
    }

    if (!(A < B + C && B < A + C && C < A + B)) {
        printf("Ceci ne correspond pas à un triangle.\n");
        return 2;
    }

    printf("Le périmètre du triangle est %lf.\n", A + B + C);

    if (A == B && B == C) {
        printf("Ce triangle est équilatéral.\n");
    } else if (A == B || A == C || B == C) {
        printf("Ce triangle est isocèle.\n");
    } else if (A*A == B*B + C*C || B*B == A*A + C*C || C*C == A*A + B*B) {
        printf("Ce triangle est rectangle.\n");
    } else {
        printf("Ce triangle est quelconque.\n");
    }


	return 0;
}
