/* Tableau des différences */

#include <stdio.h>

#define N 10

void soustraire(int A[N], int R[N], int n) {
    int i;
    for (i = 0; i < n-1; i++) {
        R[i] = A[i+1] - A[i];        
    }
}

void afficherTableau(int A[N], int n) {
    int i;
    for (i = 0; i < n; i++) {
        printf("%11i ", A[i]);
    }
    printf("\n");

}

int main() {

    int X[N] = {2,8,-3,5,9,-4,-2,0,1,16};
    int DX[N][N];
    int i;
    printf("-- X  ");
    afficherTableau(X, N);
    soustraire(X, DX[0], N);
    printf("-- D1X");
    afficherTableau(DX[0], N-1);
    for (i = 0; i < N-1; i++) {
        soustraire(DX[i], DX[i+1], N-i);
        printf("-- D%dX", i+1);
        afficherTableau(DX[i], N-i-1);
    }
    return 0;
}
