/* Moyenne */

#include <stdio.h>

double moyenne(int n, double T[n]) {
    int i;
    double tot = 0;
    for (i = 0; i < n; i++) {
        tot += T[i];
    }
    return tot/n;
}

double maximum(int n, double T[n]) {
    int i;
    double max = T[0];
    for (i = 0; i < n; i++) {
        if (T[i] > max) {
            max = T[i];
        }
    }
    return max;
}

int main() {
    int n, i;
    printf("Nombre de nombres ? ");
    scanf("%d", &n);
    double T[n];
    for (i = 0; i < n; i++) {
        printf("Saisir le nombre %d : ", i+1);
        scanf("%lf", &T[i]);
    }
    printf("Moyenne : %lf\n", moyenne(n, T));
    printf("Maximum : %lf\n", maximum(n, T));
    return 0;
}
