/* Suites entières */

#include <stdio.h>

int fibonacci(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return fibonacci(n-2) + fibonacci(n-1);
    }

}

int u(int n);
int v(int n);

int u(int n) {
    if (n == 0) {
        return 1;
    } else {
        return v(n-1)+1;
    }
}

int v(int n) {
    if (n == 0) {
        return 0;
    } else {
        return 2*u(n-1);
    }
}

int main() {

    int choix, n, res;
    printf("Quelle fonction voulez-vous utiliser ?\n 1 : fibonacci\n 2 : u\n 3 : v\nChoix : ");
    scanf("%d", &choix);
    printf("n = ");
    scanf("%d", &n);

    switch (choix) {
        case 1:
            res = fibonacci(n);
            break;
        case 2:
            res = u(n);
            break;
        case 3:
            res = v(n);
            break;
        default:
            printf("Lisez un peu ce qu'on vous dit...\n");
            return 2;
    }

    printf("Résultat : %d\n", res);

    return 0;
}
