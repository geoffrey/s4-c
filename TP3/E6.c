/* Conversion chaîne de caractères en entier */

#include <stdio.h>
#include <math.h>

#define N 20

int length(char* c) {
    int i, len = -1;
    for (i = 0; i < N; i++) {
        if (c[i] == '\0') {
            len = i;
            break;
        }
    }
    return len;
}

int number(char c) {
    return c - '0';
}

int atoiMaison(char* c) {
    int i, len = length(c), n = 0;
    for (i = 0; i < len; i++) {
        n += number(c[i]) * (int) pow(10, len - i - 1);
    }
    return n;
}

int main() {
    char c[N];
    printf("Entrez un nombre entier : ");
    scanf("%s", &c);
    printf("Vous avez entré : %d\n", atoiMaison(c));
    return 0;
}
