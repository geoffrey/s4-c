/* Maximum et position */

#include <stdio.h>

double maximum(int n, double T[n]) {
    // Même si n est inferieur à la dimension du tableau, ça marche quand même
    int i;
    double max = T[0];
    for (i = 0; i < n; i++) {
        if (T[i] > max) {
            max = T[i];
        }
    }
    return max;
}
int maximumIndice(int n, double T[n]) {
    // Même si n est inferieur à la dimension du tableau, ça marche quand même
    int i, maxIndice = 0;
    double max = T[0];
    for (i = n-1; i > 0; i--) {
        if (T[i] > max) {
            max = T[i];
            maxIndice = i;
        }
    }
    return maxIndice;
}

int main() {
    int n, i, t;
    printf("Nombre de nombres ? ");
    scanf("%d", &n);
    double T[n];
    for (i = 0; i < n; i++) {
        printf("Saisir le nombre %d : ", i+1);
        scanf("%lf", &T[i]);
    }

    printf("Sur combien de valeurs travailler ? ");
    scanf("%d", &t);
    if (t > n) {
        printf("Le nombre de valeurs sur lesquelles travailler doit être inférieur à la taille du tableau\n");
        return 2;
    }

    printf("Maximum : %lf (à l'indice %d)\n", maximum(t, T), maximumIndice(t, T)+1);
    return 0;
}
