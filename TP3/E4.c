/* Recherche d'éléments sur critère */

#include <stdio.h>

int recherche(int n, double T[n]) {
    int i;
    for (i = 0; i < n; i++) {
        if (T[i] < 0) {
            return i;
        }
    }
    return -1;
}

int main() {
    int n, i;
    printf("Nombre de nombres ? ");
    scanf("%d", &n);
    double T[n];
    for (i = 0; i < n; i++) {
        printf("Saisir le nombre %d : ", i+1);
        scanf("%lf", &T[i]);
    }
    printf("Recherche : %lf\n", recherche(n, T)+1);
    return 0;
}
