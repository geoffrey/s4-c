/* Mise en majuscules / minuscules */

#include <stdio.h>

#define N 255

int length(char* c) {
    int i, len = -1;
    for (i = 0; i < N; i++) {
        if (c[i] == '\0') {
            len = i;
            break;
        }
    }
    return len;
}

void majuscule(char* c) {
    int i, len = length(c);
    for (i = 0; i < len; i++) {
        if ('a' <= c[i] && c[i] <= 'z') {
            c[i] += 'A' - 'a';
        }
    }

}

void minuscule(char* c) {
    int i, len = length(c);
    for (i = 0; i < len; i++) {
        if ('A' <= c[i] && c[i] <= 'Z') {
            c[i] += 'a' - 'A';
        }
    }

}

int main() {
    char c[N];
    printf("Entrez du texte : ");
    scanf("%s", &c);
    majuscule(c);
    printf("Majuscules : %s (addresse : 0x%x)\n", c, &c);
    minuscule(c);
    printf("Minuscules : %s (addresse : 0x%x)\n", c, &c);
    return 0;
}
