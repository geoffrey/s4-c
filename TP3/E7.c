/* Miroir */

#include <stdio.h>

#define N 255

int length(char* c) {
    int i, len = -1;
    for (i = 0; i < N; i++) {
        if (c[i] == '\0') {
            len = i;
            break;
        }
    }
    return len;
}

char* miroir(char* c) {
    int i, len = length(c);
    static char mir[N];
    for (i = 0; i < len; i++) {
        mir[i] = c[len-i-1];
    }
    return mir;

}

int main() {
    char c[N];
    printf("Entrez du texte : ");
    scanf("%s", &c);
    char* m = miroir(c);
    printf("Miroir : %s (addresse : 0x%x)\n", m, &m);
    return 0;
}

