/* Structures et tableaux */

#include <stdio.h>
#include <string.h>

struct VIP {
    char nom[26], prenom[26];
    long dateNai, tel;
    float taille, poids;
} client[20];

void saisie(int i) {
    printf("Nom : ");
    scanf("%s", &client[i].nom);
    printf("Prénom : ");
    scanf("%s", &client[i].prenom);
    printf("Date de naissance : ");
    scanf("%ld", &client[i].dateNai);
    printf("Taille : ");
    scanf("%f", &client[i].taille);
    printf("Poids : ");
    scanf("%f", &client[i].poids);
    printf("Téléphone : ");
    scanf("%ld", &client[i].tel);

}

void lire(int i) {
    printf("Nom, Prénom : %s, %s\n", client[i].nom, client[i].prenom);
    printf("Date de naissance : %ld\n", client[i].dateNai);
    printf("Taille : %f\n", client[i].taille);
    printf("Poids : %ld\n", client[i].poids);
    printf("Téléphone : %ld\n", client[i].tel);

}

int main() {
    int i, nb;
    printf("Nombre de clients : ");
    scanf("%d", &nb);
    for (i = 0; i < nb; ++i) saisie(i);
    for (i = 0; i < nb; ++i) lire(i);
    // system("pause"); // Ne fonctionne que sous Windows
    return 0;
}
