/* Dérivées */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// 1)

double f(double x) {
    // L'utilisation d'un pointeur ici est superflue car
    // il est plus rapide et moins consommateur de
    // faire des opérations de la sorte sur une copie de x
    // plutôt que sur des références vers x
    return ( (2*x+1)*(x-1) ) / sqrt( pow(x, 2) + 1 );
}

// 2)

double deriv(double (*Pf)(double x), double x, double h) {
    return ( (*Pf)(x + h) - (*Pf)(x - h) ) / (2*h);
}

// 3)

double deriv2(double (*Pf)(double x), double x, double h) {
    double Pfd(double a) { // On définit f'(a) qui est défini selon Pf et h
        return deriv(Pf, a, h);
    }
    // return ( deriv(Pf, x+h, h) - deriv(Pf, x-h, h) ) / (2 * h); // Sans pointeur
    return deriv(Pfd, x, h); // Avec fonction
}

int main(int argc, char *argv[]) {
    // Saisie de x
    double x;
    printf("Saisissez x : ");
    scanf("%lf", &x);

    // Choix du degré de dérivation
    int choix;
    printf("Saisissez le degré de dérivation désiré pour f : ");
    scanf("%d", &choix);

    // Saisie du pas de dérivation
    double h; // On ne demmande h uniquement s'il y a dérivée, c'est inutile sinon
    if (choix > 0) {
        printf("Saisissez le pas de dérivation h : ");
        scanf("%lf", &h);
    }

    // Éxecution selon le choix
    switch (choix) {
        case 0:
            printf("f(%11lf) = %11f\n", x, f(x));
            break;

        case 1:
            printf("f'(%11lf) = %11f (précision %lf)\n", x, deriv(f, x, h), h);
            break;

        case 2:
            printf("f\"(%11lf) = %11f (précision %lf)\n", x, deriv2(f, x, h), h);
            break;

        default:
            printf("Merci de bien saisir un degré de dérivation compris entre 0 et 2.\n");
            return 1;
    }

    return 0;
}

// 4) Test

// Saisissez x : 2
// Saisissez le degré de dérivation désiré pour f : 0
// f(   2.000000) =    2.236068
// (Valeur exacte = 2.23606797749979)

// Saisissez x : 2
// Saisissez le degré de dérivation désiré pour f : 1
// Saisissez le pas de dérivation h : 0.001
// f'(   2.000000) =    2.236068 (précision 0.001000)
// (Valeur exacte = 2.23606797749979)

// Saisissez x : 2
// Saisissez le degré de dérivation désiré pour f : 2
// Saisissez le pas de dérivation h : 0.001
// f"(   2.000000) =   -0.089443 (précision 0.001000)
// (Valeur exacte = -0.089442719099992)

// 5) : Déjà inclus. Les test produisent exactement la même sortie.

