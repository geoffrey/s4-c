/* Calcule la distance entre deux points A et B */
#include <stdio.h>
#include <math.h>

int main() {
	double xa, ya, za, xb, yb, zb;
	printf("Saisissez les coordonnées du point A sous la forme X,Y,Z\n");
	scanf("%lf,%lf,%lf", &xa, &ya, &za);
	printf("Saisissez les coordonnées du point B sous la forme X,Y,Z\n");
	scanf("%lf,%lf,%lf", &xb, &yb, &zb);
	double d = sqrt(pow(xb-xa, 2) + pow(yb-ya, 2) + pow(zb-za, 2));
	printf("AB = %f\n", d);
	return 0;
}
