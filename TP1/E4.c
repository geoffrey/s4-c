/* Calcule la somme des N premiers termes de la série harmonique */
#include <stdio.h>
#include <math.h>

int main() {
	double n, s = 0, k;
	printf("Saisissez N\n");
	scanf("%lf", &n);
	for (k = 1; k <= n; k++) {
		s += 1/k; // Si k avait été un entier, 1/k aurait été un entier aussi (0)
	}
	printf("%f\n", s);
	return 0;
}
