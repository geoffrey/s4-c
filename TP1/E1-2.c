/* Lit un code ASCII et affiche le caractère correspondant */
#include <stdio.h>

int main() {
	int C;
	printf("Saisir un code numérique compris entre 0 et 127  suivi de 'Entrée'\n");
	scanf("%d", &C);
	putchar(C);
    // Équivalent à la ligne de code suivante
    // printf("%c", C);
    printf("\n");
	return 0;
}
