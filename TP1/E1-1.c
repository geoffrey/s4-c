/* Lit 1 caractère et affiche son code ASCII */
#include <stdio.h>

int main() {
	int C;
	printf("Saisir un caractère suivi de 'Entrée'\n");
	C = getchar();
	printf("Le caractère %c a le code ASCII %d\n", C, C);
	return 0;
}
