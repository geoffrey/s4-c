/* Calcule un produit scalaire */
#include <stdio.h>


int main() {
    
    double U[3]; 
    double V[3]; 

    printf("Entrez les coordonnées de U séparées par des virgules\n");
    scanf("%lf,%lf,%lf", &U[0], &U[1], &U[2]);
    printf("Entrez les coordonnées de V séparées par des virgules\n");
    scanf("%lf,%lf,%lf", &V[0], &V[1], &V[2]);

    printf("U·V=%f\n", U[0]*V[0]+U[1]*V[1]+U[2]*V[2]);

	return 0;
}
