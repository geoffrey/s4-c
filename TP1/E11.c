/* Opération sur des matrices */
#include <stdio.h>


int main() {
    int M, N;

    printf("Entrez les dimensions en M des matrices : ");
    scanf("%d", &M);
    printf("Entrez les dimensions en N des matrices : ");
    scanf("%d", &N);

    if (M < 1 || M > 50 || N < 1 || N > 50) {
        printf("M et N doivent être compris entre 1 et 50\n");
        return 2;
    }
    
    int A[50][50];
    int B[50][50];
    int m, n;

    for (n = 0; n < N; n++) {
        for (m = 0; m < M; m++) {
            printf("Entrez la valeur de la matrice A aux coordonnées (%d, %d) : ", m, n);
            scanf("%d", &A[m][n]);
        }
    }

    for (n = 0; n < N; n++) {
        for (m = 0; m < M; m++) {
            printf("Entrez la valeur de la matrice B aux coordonnées (%d, %d) : ", m, n);
            scanf("%d", &B[m][n]);
        }
    }

    int choix;
    printf("Tapez 0 pour calculer A+B ; 1 pour calculer A×B : ");
    scanf("%d", &choix);

    int T[50][50];

    printf("\n");
    if (!choix) {
        printf("A+B");
        for (n = 0; n < N; n++) {
            for (m = 0; m < M; m++) {
                T[m][n] = A[m][n] + B[m][n];
            }
        }
    } else {
        int i, S;
        if (M != N) {
            printf("La largeur de A doit être égale à la hauteur de B");
            return 2;

        }
        printf("A×B");
        for (n = 0; n < N; n++) {
            for (m = 0; m < M; m++) {
                S = 0;
                for (i = 0; i < M; i++) {
                    S += A[i][n] * B[m][i];
                }
                T[m][n] = S;
            }
        }
    }

    for (m = 0; m < M; m++) {
        printf("│");
        printf("%3d", m);
    }
    for (n = 0; n < N; n++) {
        // Dessine une ligne
        printf("\n───");
        for (m = 0; m < M; m++) {
            printf("┼───");
        }
        printf("\n");

        printf("%3d", n);
        for (m = 0; m < M; m++) {
            printf("│");
            printf("%3d", T[m][n]);
        }
    }
    printf("\n");
	return 0;
}
