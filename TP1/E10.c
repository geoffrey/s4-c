/* Somme les éléments d'un tableau */
#include <stdio.h>


int main() {
    int M, N;

    printf("Entrez les dimensions en M du tableau : ");
    scanf("%d", &M);
    printf("Entrez les dimensions en N du tableau : ");
    scanf("%d", &N);

    if (M < 1 || M > 50 || N < 1 || N > 50) {
        printf("M et N doivent être compris entre 1 et 50\n");
        return 2;
    }
    
    int t[50][50];
    int m, n;

    for (n = 0; n < N; n++) {
        for (m = 0; m < M; m++) {
            printf("Entrez la valeur du tableau aux coordonnées (%d, %d) : ", m, n);
            scanf("%d", &t[m][n]);
        }
    }

    printf("\n T ");
    for (m = 0; m < M; m++) {
        printf("│");
        printf("%3d", m);
    }
    for (n = 0; n < N; n++) {
        // Dessine une ligne
        printf("\n───");
        for (m = 0; m < M; m++) {
            printf("┼───");
        }
        printf("\n");

        printf("%3d", n);
        for (m = 0; m < M; m++) {
            printf("│");
            printf("%3d", t[m][n]);
        }
    }
    printf("\n\n");

    int S;
    // S_L
    for (n = 0; n < N; n++) {
        S = 0;
        for (m = 0; m < M; m++) {
            S += t[m][n];            
        }
        printf("S_L[%d] = %d\n", n, S);
    }
    // S_C
    for (m = 0; m < M; m++) {
        S = 0;
        for (n = 0; n < N; n++) {
            S += t[m][n];            
        }
        printf("S_C[%d] = %d\n", m, S);
    }

	return 0;
}
