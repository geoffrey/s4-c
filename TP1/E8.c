/* Affiche la somme due pour un prêt */
#include <stdio.h>


int main() {
    
    float S0 = 8000;
    float t = 0.005;
    float x = 250;


    float S = S0;
    int m;
    for (m = 1; m <= 12; m++) {
        S = S * (1 + t) - x;
        printf("Au mois %d, vous devrez %f€ à vôtre banque\n", m, S);
    }

    S = S0;
    float Sn = S; // Sn : prochaine mensualité
    m = 0;
    while (Sn > 0) {
        m++;
        S = Sn;
        Sn = S * (1 + t) - x;
    }

    m--; // Dans la boucle m prend une mensualité en trop

    printf("Au mois %d, il vous restera %f€ à payer.\n", m, S);


	return 0;
}
