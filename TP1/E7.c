/* Affiche une table de multiplication */
#include <stdio.h>

void afficherDansBase(int nombre, int base) {
    int a, b, c; // Chiffres
    c = nombre % base;
    nombre = nombre / base;
    if (nombre == 0) {
        printf("  %x", c);
        return;
    }
    b = nombre % base;
    nombre = nombre / base;
    if (nombre == 0) {
        printf(" %x%x", b, c);
        return;
    }
    a = nombre % base;
    printf("%x%x%x", a, b, c);
    return;
}

int main() {
    int b, x, y;
	printf("Quelle base ?\n");
	scanf("%d", &b);
    if (b < 2 || b > 16) {
        printf("La base doit être comprise entre 2 et 16\n");
        return 2;
   }
    printf("X×Y");
    for (x = 0; x <= b; x++) {
        printf("│");
        afficherDansBase(x, b);
    }
    for (y = 0; y <= b; y++) {
        // Dessine une ligne
        printf("\n───");
        for (x = 0; x <= b; x++) {
            printf("┼───");
        }
        printf("\n");

        afficherDansBase(y, b);
        for (x = 0; x <= b; x++) {
            printf("│");
            afficherDansBase(x*y, b);
        }
    }
    printf("\n");

	return 0;
}
