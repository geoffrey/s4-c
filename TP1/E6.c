/* Calcule la racine carrée de A par récurrence */
#include <stdio.h>

int main() {
	double a, x;
    int jmax, j;
	printf("Saisissez A positif\n");
	scanf("%lf", &a);
    if (a <= 0) {
        printf("On a dit A>0 !\n");
        return 1;
    }

    printf("Combien d'itérations ?\n");
    scanf("%d", &jmax);
    if (jmax < 1 || jmax > 50) {
        printf("Le nombre d'itérations doit être compris entre 1 et 50.\n");
        return 1;
    }

    x = a;
    for (j = 1; j <= jmax; j++) {
        x = (x + a/x)/2;
        printf("La %dème approximation de la racine  carrée de %f est %f.\n", j, a, x);
    }
    printf("%f\n", x);
	return 0;
}
