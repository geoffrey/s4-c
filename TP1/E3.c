/* Affiche les solutions à une équation du second degré */
#include <stdio.h>
#include <math.h>
#include <complex.h>

int main() {
	double a, b, c;
	printf("Saisissez les réels a≠0, b et c séparés par une virgule\n");
	scanf("%lf,%lf,%lf", &a, &b, &c);
	if (a == 0) {
		printf("On a dit a≠0 !\n");
	} else {
		double d = pow(b,2)-4*a*c;
		printf("%f\n", d);
		if (d == 0) {
			printf("x0 = %f", -b/2*a);
		} else if (d > 0) {
			printf("x1 = %f; x2 = %f", (-b+sqrt(d))/2*a, (-b-sqrt(d))/2*a);
		} else { 
            double complex z1 = pow(b,2)/2*a + sqrt(-d)/2*a * I;
            double complex z2 = pow(b,2)/2*a - sqrt(-d)/2*a * I;
			printf("z1 = %f+i%f; z2 = %f+i%f\n", creal(z1), cimag(z1), creal(z2), cimag(z2));

		}
	}
	return 0;
}
