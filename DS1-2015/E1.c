/* Calcul de la mensualité d'emprunt */
#include <stdio.h>
#include <math.h>

int main() {
    
    double S = 10000;
    double t = 0.036;
    double x = t / 12;
    int n = 30;
    double R;

    R = (S*x)/(1-pow(1+x, -n));

    printf("2) %lf\n", R);

	return 0;
}
