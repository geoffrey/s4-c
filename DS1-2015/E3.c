/* Nombres hypercomplexes */
#include <stdio.h>
#include <math.h>

// 1)

typedef struct complexe {
    double a, b;
} complexe;

// 2)

complexe Csom(complexe z1, complexe z2) {
    complexe Z;
    Z.a = z1.a + z2.a;
    Z.b = z1.b + z2.b;
    return Z;
}

// 3)

complexe Cprod(complexe z1, complexe z2) {
    complexe Z;
    Z.a = z1.a * z2.a - z1.b * z2.b;
    Z.b = z1.a * z2.b + z1.b * z2.a;
    return Z;
}

// 4)

int main4() {
    complexe z1, z2;


    printf("Saisissez la partie réelle de z1 : ");
    scanf("%lf", &z1.a);
    printf("Saisissez la partie immaginaire de z1 : ");
    scanf("%lf", &z1.b);

    printf("Saisissez la partie réelle de z2 : ");
    scanf("%lf", &z2.a);
    printf("Saisissez la partie immaginaire de z2 :");
    scanf("%lf", &z2.b);

    complexe som = Csom(z1, z2);
    complexe prod = Cprod(z1, z2);
    printf("z1 + z2 = %lf + %lfi\n", som.a, som.b);
    printf("z1 × z2 = %lf + %lfi\n", prod.a, prod.b);


    // Pour z1 = -3 + 4i et z2 = 6 - 8i
    // z1 + z2 = 3.000000 + -4.000000i
    // z1 × z2 = 14.000000 + 12.000000i

	return 0;
}

// 5)

typedef struct qnion {
    complexe z1, z2;
} qnion;

// 6)

float Qmod(qnion q) {
    return sqrt(pow(q.z1.a, 2) + pow(q.z1.b, 2) + pow(q.z2.a, 2) + pow(q.z2.b, 2));
}

complexe Cinv(complexe z) {
    complexe Z;
    Z.a = -z.a;
    Z.b = -z.b;
    return Z;
}

int main6() {
    qnion q1, q2;
    
    printf("Quaternion q1\n");
    printf("Saisissez la partie réelle de z11 : ");
    scanf("%lf", &q1.z1.a);
    printf("Saisissez la partie immaginaire de z11 : ");
    scanf("%lf", &q1.z1.b);
    printf("Saisissez la partie réelle de z21 : ");
    scanf("%lf", &q1.z2.a);
    printf("Saisissez la partie immaginaire de z21 :");
    scanf("%lf", &q1.z2.b);

    printf("Quaternion q2\n");
    printf("Saisissez la partie réelle de z12 : ");
    scanf("%lf", &q2.z1.a);
    printf("Saisissez la partie immaginaire de z12 : ");
    scanf("%lf", &q2.z1.b);
    printf("Saisissez la partie réelle de z22 : ");
    scanf("%lf", &q2.z2.a);
    printf("Saisissez la partie immaginaire de z22 :");
    scanf("%lf", &q2.z2.b);

    printf("\n");
    printf("Module de q1 : %lf\n", Qmod(q1));
    printf("Module de q2 : %lf\n", Qmod(q2));

    qnion P;
    P.z1 = Csom(q1.z1, q2.z1);
    P.z2 = Csom(q1.z2, q2.z2);
    printf("q1 + q2 = (%lf, %lf, %lf, %lf)\n", P.z1.a, P.z1.b, P.z2.a, P.z2.b);

    qnion Q;
    Q.z1 = Csom(Cprod(q1.z1, q2.z1), Cinv(Cprod(q1.z2, q2.z2)));
    Q.z2 = Csom(Cprod(q1.z1, q2.z2), Cprod(q2.z1, q1.z2));
    printf("q1 × q2 = (%lf, %lf, %lf, %lf)\n", Q.z1.a, Q.z1.b, Q.z2.a, Q.z2.b);

}

int main() {
    main6();
}
