/* Triangle de Pascal */

#include <stdio.h>

int main() {

    int N;
    int x, y;
    printf("Saisir N : ");
    scanf("%d", &N);

    // On crée un tableau, et on le remplit de 0
    int T[N+1][N+1];
    for (y = 0; y <= N; y++) {
        for (x = 1; x < N; x++) {
            T[x][y] = 0;
        }
        T[0][y] = 1; // On met des 1 sur la première colonne
    }

    // On calcule les sommes
    for (y = 0; y <= N; y++) {
        for (x = 1; x <= y; x++) {
            T[x][y] = T[x-1][y-1] + T[x][y-1];
        }
    }
    
    // On affiche le tableau
    for (y = 0; y <= N; y++) {
        for (x = 0; x < N; x++) {
            if (x <= y) {
                printf("%11d", T[x][y]);
            }
        }
        printf("\n");
    }
    

    return 0;
}
