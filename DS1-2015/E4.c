/* Algorithme de Thomas */

#include <stdio.h>

// Pas testé

// 1)

double D3diag(int n, double* a, double* b, double* c) {
    double D[n];
    int i;
    D[0] = b[0];
    D[1] = b[0] * b[1] - a[1] * c[0];
    for (i = 2; i < n; i++) {
        D[i] = b[i] * D[i-1] - a[i] * c[i-1] * D[i-2];
    }
    return D[i];
}

// 2)

#define N_MAX 100

int main() {
    // -
    int n;
    printf("Saisir la dimension n du système : ");
    scanf("%d", &n);

    if (n < 3 || n > N_MAX) {
        printf("N doit doit être compris entre 3 et %d", N_MAX);
    }

    // -
    double a[n], b[n], c[n], y[n];

    int i;
    for (i = 1; i < n; i++) {
        printf("Saisissez l'élément %d de la diagonale A : ", i);
        scanf("%lf", &a[i]);
    }
    for (i = 0; i < n; i++) {
        printf("Saisissez l'élément %d de la diagonale B : ", i);
        scanf("%lf", &b[i]);
    }
    for (i = 0; i < n-1; i++) {
        printf("Saisissez l'élément %d de la diagonale C : ", i);
        scanf("%lf", &c[i]);
    }
    for (i = 0; i < n; i++) {
        printf("Saisissez l'élément %d du second membre y : ", i);
        scanf("%lf", &y[i]);
    }

    // -
    double G[n], B[n], x[n];

    G[0] = c[0] / b[0];
    B[0] = y[0] / b[0];

    for (i = 1; i < n-1; i++) {
        G[i] = c[i] / (b[i] - a[i] * G[i-1]);
    }

    for (i = 1; i < n; i++) {
        B[i] = (y[i] - a[i] * B[i-1]) / (b[i] - a[i] * G[i-1]);
    }

    x[n-1] = B[n-1];
    for (i = n-2; i >= 0; i--) {
        x[i] = B[i] - G[i] * x[i+1];
    }

    // -
    printf("Déterminant de T : %lf\n", D3diag(n, a, b, c));

    printf("X = ");
    for (i = 1; i < n; i++) {
        printf("%11lf ", x[i]);
    }
    printf("\n");

}
